$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "active_record/type/ipaddress/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "activerecord-type-ipaddress"
  s.version     = ActiveRecord::Type::Ipaddress::VERSION
  s.authors     = ["inye <me@inye.cc>"]
  s.email       = ["me@inye.cc"]
  s.homepage    = "https://gitlab.com/inye/activerecord-type-ip_address"
  s.summary     = "ActiveRecord plugin to keep IPAddress object inside integer DB column"
  s.description = s.summary
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.1"

  s.add_development_dependency "sqlite3"
end
