# ActiveRecord::Type::IPAddress
ActiveRecord plugin to keep IP Address object inside integer database column. Uses brilliant
[ipaddress](https://github.com/ipaddress-gem/ipaddress) gem for IPAddress class.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'activerecord-type-ipaddress'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install activerecord-type-ipaddress
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
